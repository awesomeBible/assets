# awesomeBible/assets

Dieses Repository enthält einige Dateien für das awesomeBible-Branding zum Download.

## Branding Richtlinien und weitere Assets sind in den [awesomeBible Docs](https://docs.awesomebible.de/branding/) zu finden.